default: stats_base

all: root_base stats_base stats_image

# global build variables
BASE_IMAGE=python:3.10-slim-bullseye
ROOT_VERSION=6-28-06
# TARGET_BRANCH may be a branch or a tag
TARGET_BRANCH="v${ROOT_VERSION}"
GIT_PROJECT_URL=https://github.com/atlascollaboration/root
ROOT_VERSION_SCM=$(subst -,.,$(ROOT_VERSION))

root_base:
	docker build . \
	-f root-base/Dockerfile \
	--build-arg BASE_IMAGE=$(BASE_IMAGE) \
	--build-arg TARGET_BRANCH=$(TARGET_BRANCH) \
	--build-arg GIT_PROJECT_URL=$(GIT_PROJECT_URL) \
	--build-arg XROOTD_TAG=v5.5.4 \
	--tag atlasamglab/root-base:root$(ROOT_VERSION_SCM) \
	--tag atlasamglab/root-base:root$(ROOT_VERSION_SCM)-python3.10 \
	--tag atlasamglab/root-base:latest

stats_base:
	docker build . \
	-f stats-base/Dockerfile \
	--build-arg BUILDER_IMAGE=atlasamglab/root-base:root$(ROOT_VERSION_SCM) \
	--build-arg TARGET_BRANCH=$(TARGET_BRANCH) \
	--build-arg GIT_PROJECT_URL=$(GIT_PROJECT_URL) \
	--tag atlasamglab/stats-base:root$(ROOT_VERSION_SCM) \
	--tag atlasamglab/stats-base:root$(ROOT_VERSION_SCM)-python3.10 \
	--tag atlasamglab/stats-base:latest

stats_image:
	docker build . \
		-f stats/Dockerfile \
		--build-arg BASE_IMAGE=atlasamglab/stats-base:root$(ROOT_VERSION_SCM) \
		--tag atlasamglab/stats:root$(ROOT_VERSION_SCM) \
		--tag atlasamglab/stats:root$(ROOT_VERSION_SCM)-python3.10 \
		--tag atlasamglab/stats:latest

debug_root_base:
	docker build . \
	-f root-base/Dockerfile \
	--build-arg BASE_IMAGE=$(BASE_IMAGE) \
	--build-arg TARGET_BRANCH=$(TARGET_BRANCH) \
	--build-arg GIT_PROJECT_URL=$(GIT_PROJECT_URL) \
	--build-arg XROOTD_TAG=v5.5.4 \
	--tag atlasamglab/root-base:debug-local

debug_stats_base:
	docker build . \
	-f stats-base/Dockerfile \
	--build-arg BUILDER_IMAGE=atlasamglab/root-base:debug-local \
	--build-arg TARGET_BRANCH=$(TARGET_BRANCH) \
	--build-arg GIT_PROJECT_URL=$(GIT_PROJECT_URL) \
	--tag atlasamglab/stats-base:debug-local

run_root_base:
	docker run --rm -it atlasamglab/root-base:latest

run_stats_base:
	docker run --rm -it atlasamglab/stats-base:latest

run_stats:
	docker run --rm -it atlasamglab/stats:latest
